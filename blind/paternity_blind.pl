#!/usr/bin/env perl
use strict;
use warnings;



#my @DGRPfields=(chrom,position,line_101,line_105,line_109,line_129,line_136,line_138,line_142,line_149,line_153,line_158,line_161,line_176,line_177,line_181,line_195,line_208,line_21,line_217,line_227,line_228,line_229,line_233,line_235,line_237,line_239,line_256,line_26,line_28,line_280,line_287,line_301,line_303,line_304,line_306,line_307,line_309,line_310,line_313,line_315,line_317,line_318,line_320,line_321,line_324,line_325,line_332,line_335,line_338,line_350,line_352,line_356,line_357,line_358,line_359,line_360,line_362,line_365,line_367,line_370,line_371,line_374,line_373,line_375,line_377,line_379,line_38,line_380,line_381,line_383,line_386,line_391,line_392,line_399,line_40,line_405,line_406,line_409,line_41,line_42,line_426,line_427,line_437,line_439,line_440,line_441,line_443,line_45,line_461,line_486,line_49,line_491,line_492,line_502,line_508,line_509,line_513,line_517,line_531,line_535,line_555,line_563,line_57,line_589,line_59,line_591,line_595,line_639,line_642,line_646,line_83,line_703,line_705,line_707,line_712,line_714,line_716,line_721,line_727,line_73,line_730,line_732,line_737,line_738,line_75,line_757,line_761,line_765,line_774,line_776,line_783,line_786,line_787,line_790,line_796,line_799,line_801,line_802,line_804,line_805,line_808,line_810,line_812,line_818,line_820,line_822,line_69,line_832,line_837,line_85,line_852,line_855,line_857,line_859,line_861,line_879,line_88,line_882,line_884,line_887,line_890,line_892,line_894,line_897,line_907,line_908,line_91,line_911,line_93,line_Ref);

# in the first part of script, store all DGRP info into a hash
my %freqhash=();
my $file = "SNPs.dgrp";
open(FILE, $file) or die("Could not open SNP file.");
foreach my $line (<FILE>) {
    chomp($line);
    my @fields=split(/,/,$line);
    my $chrom=$fields[0];
    my $position=$fields[1];
    my $poskey=join("_", ($chrom,$position));    
    my $line_Ref=$fields[170];
    my $ac=0;
    for (my $i=2; $i<=169; $i++) {
	if (defined $fields[$i] && $fields[$i] ne $line_Ref) {
	    $ac++;
	}
    }
    my $af=($ac/168);
    push(@fields, $af);
    my $storedDGRP=join(",", @fields);
    $freqhash{ $poskey } = $storedDGRP;
    open (LOGFILE, '>>paternity_blind.log');
    print LOGFILE $poskey, "\t", $af, "\n";
}
close(FILE);
close(LOGFILE);

# in the second part of the script, cycle through called SNPs in data
my $file2 = "JPRB.samples";
open(FILE2, $file2) or die("Could not open data file.");
foreach my $line2 (<FILE2>) {
    chomp($line2);
    my @columns=split(/\s+/, $line2);
    my $chrom=$columns[0];
    my $position=$columns[1];
    my $poskey=join("_", ($chrom,$position));
    my $ref=$columns[2];
    my $alt=$columns[3];
    my $qual=$columns[4];
    if (defined $freqhash{ $poskey }) {
        my @dgrp_info=split(",", $freqhash{ $poskey });
	if ($dgrp_info[171] < 0.1) {
	    print $poskey, "\t" , $dgrp_info[171], "\t";
	    for (my $j=5; $j<=198; $j++) {
# if the sample cannot be genotyped, print NA
		my @samplefields=split(":", $columns[$j]);
		if ($samplefields[0] eq "./.") { 
		    print "NA";
		}
		elsif ($samplefields[0] eq "0/0" || $samplefields[0] eq "0/1" || $samplefields[0] eq "1/1") { 
		    my @reads=split(",", $samplefields[1]);
		    my $refcounts=$reads[0];
		    my $altcounts=$reads[1];
# if the sample has the alternative allele, print 1
		    if ($altcounts > 0) {
			print "1";
		    }
# if it does not, print a 0
		    else {
			print "0";
		    }
		}
		print "\t";
	    }
	    print "\n";
	}    
    }
}

close(FILE2);
