#!/usr/bin/env perl
use strict;
use warnings;
use List::Util qw(first max maxstr min minstr reduce shuffle sum);

my @DGRPfields=("chrom","position","DGRP-021","DGRP-026","DGRP-028","DGRP-031","DGRP-032","DGRP-038","DGRP-040","DGRP-041","DGRP-042","DGRP-045","DGRP-048","DGRP-049","DGRP-057","DGRP-059","DGRP-069","DGRP-073","DGRP-075","DGRP-083","DGRP-085","DGRP-088","DGRP-091","DGRP-093","DGRP-100","DGRP-101","DGRP-105","DGRP-109","DGRP-129","DGRP-136","DGRP-138","DGRP-142","DGRP-149","DGRP-153","DGRP-158","DGRP-161","DGRP-176","DGRP-177","DGRP-181","DGRP-189","DGRP-195","DGRP-208","DGRP-217","DGRP-223","DGRP-227","DGRP-228","DGRP-229","DGRP-233","DGRP-235","DGRP-237","DGRP-239","DGRP-256","DGRP-280","DGRP-287","DGRP-301","DGRP-303","DGRP-304","DGRP-306","DGRP-307","DGRP-309","DGRP-310","DGRP-313","DGRP-315","DGRP-317","DGRP-318","DGRP-319","DGRP-320","DGRP-321","DGRP-324","DGRP-325","DGRP-332","DGRP-335","DGRP-336","DGRP-338","DGRP-340","DGRP-348","DGRP-350","DGRP-352","DGRP-354","DGRP-355","DGRP-356","DGRP-357","DGRP-358","DGRP-359","DGRP-360","DGRP-361","DGRP-362","DGRP-365","DGRP-367","DGRP-370","DGRP-371","DGRP-373","DGRP-374","DGRP-375","DGRP-377","DGRP-379","DGRP-380","DGRP-381","DGRP-382","DGRP-383","DGRP-385","DGRP-386","DGRP-390","DGRP-391","DGRP-392","DGRP-395","DGRP-397","DGRP-399","DGRP-405","DGRP-406","DGRP-409","DGRP-426","DGRP-427","DGRP-437","DGRP-439","DGRP-440","DGRP-441","DGRP-443","DGRP-461","DGRP-486","DGRP-491","DGRP-492","DGRP-502","DGRP-505","DGRP-508","DGRP-509","DGRP-513","DGRP-517","DGRP-528","DGRP-530","DGRP-531","DGRP-535","DGRP-551","DGRP-555","DGRP-559","DGRP-563","DGRP-566","DGRP-584","DGRP-589","DGRP-595","DGRP-596","DGRP-627","DGRP-630","DGRP-634","DGRP-639","DGRP-642","DGRP-646","DGRP-703","DGRP-705","DGRP-707","DGRP-712","DGRP-714","DGRP-716","DGRP-721","DGRP-727","DGRP-730","DGRP-732","DGRP-737","DGRP-738","DGRP-748","DGRP-757","DGRP-761","DGRP-765","DGRP-774","DGRP-776","DGRP-783","DGRP-786","DGRP-787","DGRP-790","DGRP-796","DGRP-799","DGRP-801","DGRP-802","DGRP-804","DGRP-805","DGRP-808","DGRP-810","DGRP-812","DGRP-818","DGRP-819","DGRP-820","DGRP-821","DGRP-822","DGRP-832","DGRP-837","DGRP-843","DGRP-849","DGRP-850","DGRP-852","DGRP-853","DGRP-855","DGRP-857","DGRP-859","DGRP-861","DGRP-879","DGRP-882","DGRP-884","DGRP-887","DGRP-890","DGRP-892","DGRP-894","DGRP-897","DGRP-900","DGRP-907","DGRP-908","DGRP-911","DGRP-913","line_ref");


# in the first part of script, store all DGRP info into a hash
my %freqhash=();
my $file = "SNPs.freeze2.dgrp";
open(FILE, $file) or die("Could not open SNP file.");
foreach my $line (<FILE>) {
    chomp($line);
    my @fields=split(/,/,$line);
    my $chrom=$fields[0];
    my $position=$fields[1];
    my $poskey=join("_", ($chrom,$position));    
    my $line_Ref=$fields[207];
    my $ac=0;
    my $n_counter = 0;
    for (my $i=2; $i<=206; $i++) {
	if ($fields[$i] eq 'N') {
            $n_counter++;
        }
	if (defined $fields[$i] && $fields[$i] ne $line_Ref) {
	    $ac++;
	}
    }
    my $af=($ac/205);
    push(@fields, $ac);
    push(@fields, $n_counter);
    my $storedDGRP=join(",", @fields);
    $freqhash{ $poskey } = $storedDGRP;
    open (LOGFILE, '>>allele_freq_deconvolute_pass.log');
    print LOGFILE $poskey, "\t", $ac, "\t", $n_counter,"\n";
}
close(FILE);
close(LOGFILE);

# in the second part of the script, cycle through called SNPs in data
my $file2 = "JPRB.pass3.samples";
open(FILE2, $file2) or die("Could not open data file.");
foreach my $line2 (<FILE2>) {
    chomp($line2);
    my @columns=split(/\s+/, $line2);
    my $chrom=$columns[0];
    my $position=$columns[1];
    my $poskey=join("_", ($chrom,$position));
    my $ref=$columns[2];
    my $alt=$columns[3];
    my $qual=$columns[4];
    my @randarray=(0,1);
    if (defined $freqhash{ $poskey }) {
        my @dgrp_info=split(",", $freqhash{ $poskey });
	if ($ref eq $dgrp_info[207] && $dgrp_info[209] < 10 && ($dgrp_info[41] ne "R" && $dgrp_info[41] ne "Y" && $dgrp_info[41] ne "S" && $dgrp_info[41] ne "W" && $dgrp_info[41] ne "K" && $dgrp_info[41] ne "M") && ($dgrp_info[49] ne "R" && $dgrp_info[49] ne "Y" && $dgrp_info[49] ne "S" && $dgrp_info[49] ne "W" && $dgrp_info[49] ne "K" && $dgrp_info[49] ne "M") && ($dgrp_info[131] ne "R" && $dgrp_info[131] ne "Y" && $dgrp_info[131] ne "S" && $dgrp_info[131] ne "W" && $dgrp_info[131] ne "K" && $dgrp_info[131] ne "M")) {
	    print $chrom, "\t", $position,"\t", $dgrp_info[208], "\t";
# here we check which DGRP line the SNP belongs to, and append an identifier
	    if ($alt eq $dgrp_info[49] && $ref eq $dgrp_info[41] && $ref eq $dgrp_info[131]) {
		print "maternal", "\t";
	    }
            elsif ($alt eq $dgrp_info[49] && $alt eq $dgrp_info[41] && $alt eq $dgrp_info[131] && $dgrp_info[208] > 155) {
                print "ywcnbw", "\t";
            }
	    elsif (($alt eq $dgrp_info[49] && $alt eq $dgrp_info[41] && $ref eq $dgrp_info[131]) || ($alt eq $dgrp_info[49] && $alt eq $dgrp_info[131] && $ref eq $dgrp_info[41]) || ($alt eq $dgrp_info[41] && $alt eq $dgrp_info[131] && $ref eq $dgrp_info[49])) {
		print "multiple_matches", "\t";
	    }
	    elsif ($alt eq $dgrp_info[41] && $ref eq $dgrp_info[131] && $ref eq $dgrp_info[49]) {
		print "paternal_208", "\t";
	    }
	    elsif ($ref eq $dgrp_info[41] && $alt eq $dgrp_info[131] && $ref eq $dgrp_info[49]) {
		print "paternal_535", "\t";
            }
	    else {
		print "no_match", "\t";
	    }
	    my $counter=0;
	    my $na_counter=0;
	    for (my $j=5; $j<=71; $j++) {
		my @samplefields=split(":", $columns[$j]);
# if the sample is not genotyped, print NA
		if ($samplefields[0] eq "./.") { 
#		    print "NA";
		    $na_counter++;
		}
		elsif ($samplefields[0] eq "0/0" || $samplefields[0] eq "0/1" || $samplefields[0] eq "1/1") { 
		    my @reads=split(",", $samplefields[1]);
		    my $refcounts=$reads[0];
		    my $altcounts=$reads[1];
		    if ($altcounts > 0 && $refcounts == 0) {
#			print "1";
			$counter++;
		    }    
		    elsif ($altcounts == 0 && $refcounts > 0) {
#			print "0";
		    }
		    elsif ($altcounts > 0 && $refcounts > 0) {
			@randarray = shuffle(@randarray);
			if ($randarray[0]==1) {
#			    print $randarray[0];
			    $counter++;
			}
		    }
		    
		}
#		print "\t";
	    }
	    my $frequency=$counter/(67-$na_counter);
	    print $na_counter, "\t";
	    print $frequency;
	    print "\n";
	}    
    }
}

close(FILE2);
