#!/bin/sh

perl allele_freq_deconvolute_females.pl > allele_freq_deconvolute_females.out

perl allele_freq_deconvolute_males.pl > allele_freq_deconvolute_males.out 

perl allele_freq_deconvolute_pass1.pl > allele_freq_deconvolute_pass1.out 

perl allele_freq_deconvolute_pass2.pl >allele_freq_deconvolute_pass2.out

perl allele_freq_deconvolute_pass3.pl >allele_freq_deconvolute_pass3.out

perl allele_freq_deconvolute_pass4.pl >allele_freq_deconvolute_pass4.out

perl allele_freq_deconvolute.pl >allele_freq_deconvolute.out