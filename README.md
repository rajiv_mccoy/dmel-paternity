dmel-paternity
==============

Exploratory analysis of high throughput genomic methods of detecting paternity in Drosophila melanogaster.
